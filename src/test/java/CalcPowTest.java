import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class CalcPowTest {

    private CalcPow calcPow;

    @BeforeEach
    void setUp() {
        calcPow = new CalcPow();
    }

    @ParameterizedTest
    @CsvSource(value = {
            "2:2:4",
            "1:0:1",
            "0:20:0",
            "9:1:9"
    }, delimiter = ':')
    void powValue(double index, int power, double expected) {
        assertEquals(
                expected,
                calcPow.powValue(index, power)
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
            "2:-2:0.25"
    }, delimiter = ':')
    void powValueMinus(double index, int power, double expected) {
        assertEquals(
                expected,
                calcPow.powValue(index, power)
        );
    }
}