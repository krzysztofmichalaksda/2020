public class CalcDiv {
    double divValue(double val1, double val2) throws Exception {
        if (val2 == 0) {
            throw new Exception();
        }
        return val1 / val2;
    }
}
